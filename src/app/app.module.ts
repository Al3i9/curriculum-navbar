import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { APP_ROUTING } from './app.routers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/*Componetes*/
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { MenuComponent } from './component/menu/menu.component';
import { ContactosComponent } from './component/contactos/contactos.component';
import { CrearUsuarioComponent } from './component/menu/crear-usuario/crear-usuario.component';
import { VerUsuarioComponent } from './component/menu/ver-usuario/ver-usuario.component';

/*Angular Material*/



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MenuComponent,
    ContactosComponent,
    CrearUsuarioComponent,
    VerUsuarioComponent,
    
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
