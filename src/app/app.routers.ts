
import { RouterModule,Routes } from "@angular/router";
import { InicioComponent } from "./component/inicio/inicio.component";
import { MenuComponent } from "./component/menu/menu.component";
import { ContactosComponent } from "./component/contactos/contactos.component";


const APP_ROUTES: Routes = [
     { path: 'inicio', component: InicioComponent },
     { path: 'menu', component: MenuComponent},
     { path: 'contactos', component: ContactosComponent},
     { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
    ];
    export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
    