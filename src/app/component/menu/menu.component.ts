import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MenuService } from 'src/app/services/menu.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, AfterViewInit{

  listMenu: UsuarioDataI[] = [];
  displayedColumns: string[] = ['usuario', 'nombre', 'apellido', 'sexo', 'acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cargarUsuarios(){
    this.listMenu = this._menuService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listMenu);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(private _menuService: MenuService,
              private _snackbar: MatSnackBar,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(usuario: string){
    const opcion = confirm('Estas seguro de eliminar el usuario');
    if(opcion){
      console.log(usuario);

      this._menuService.eliminarUsuario(usuario);
      this.cargarUsuarios();

      this._snackbar.open('El usuario fue eliminado con exito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
      
    }
  }

  verUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['menu/ver-usuario', usuario]);
  }

  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['menu/crear-usuario', usuario]);
  }

}
