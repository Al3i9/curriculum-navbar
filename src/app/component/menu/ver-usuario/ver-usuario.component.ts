import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {

  form! : FormGroup;

  constructor(private activateRoute: ActivatedRoute,
              private menuService: MenuService,
              private router: Router,
              private fb: FormBuilder) { 

    this.activateRoute.params.subscribe(params =>{
      const id = params['id'];
      console.log(id);
      const usuario = this.menuService.buscarUsuario(id);
      console.log(usuario);
      
      console.log(Object.keys(usuario).length);

      if(Object.keys(usuario).length === 0){
        this.router.navigate(['/menu']);
      }

      this.form = this.fb.group({
        usuario: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        sexo: ['', Validators.required]
      });

      this.form.patchValue({
        usuario: usuario.usuario,
        nombre: usuario.nombre,
        apellido: usuario.apellido,
        sexo: usuario.sexo
      });
    });
    }

  ngOnInit(): void {
  }

  Volver(): void{
    this.router.navigate(['/menu']);
  }

}
